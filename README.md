# 15.072 Advanced Analytics Edge 

<img src="cover-picture.jpg" alt="analytics" width="400">

## Description

The amount of data available to organizations has been growing as never before, and companies and individuals who harness this data through the use of data analytics gain a critical edge in their business domain. In this class we examine how data analytics is used to transform businesses and industries, using examples and case studies in e-commerce, healthcare, social media, high technology, sports, the internet, and beyond. Through these examples and many more, we teach and demonstrate the use of analytics methods such as linear regression, logistic regression, classification trees, random forests, text analytics, social network analysis, time series modeling, clustering, optimization, and machine learning.

## Prerequisites

<i>15.060: Data, Models and Decisions</i>, or a basic statistics and a basic optimization course. The most important prerequisite is perhaps some basic experience programming. Please contact the course instructors with questions about appropriate prerequisites.

## Readings

There is no required textbook for the course.  However, we have some suggested readings from <a href="https://www.dynamic-ideas.com/books/kgsni67q285zmpdit17ix1cwbnuqac">The Analytics Edge</a> by Dimitris Bertsimas, Allison O’Hair and William Pulleyblank, Dynamic Ideas LLC, 2016. We refer to this book as the “AE book” below.

## Team

**Bart Van Parys (Instructor)**
- Office: E62-569
- Email: [vanparys@mit.edu](mailto:vanparys@mit.edu)

**Kayhan Behdin**
- Email: [behdink@mit.edu](mailto:behdink@mit.edu)

**Gabriel Isaac Afriat**
- Email: [afriatg@mit.edu](mailto:afriatg@mit.edu)

**Haoyue Wang**
- Email: [haoyuew@mit.edu](mailto:haoyuew@mit.edu)

**Brian Liu**
- Email: [briliu@mit.edu](mailto:briliu@mit.edu)


## Agenda

**Lectures:** Monday and Wednesday, 1.00pm-2.30am at E51-345

**Recitations:** 
Friday 9:00am-10:00am in E51-345

Recitations will consist of interactive sessions that will cover additional examples of the analytics methods presented in the lectures, and -- most importantly – recitations will be used to show how to create and use models in R. Recitation attendance is not mandatory it is very highly recommended. All recitations are run by the Teaching Assistant.

**Office Hours:**
- TAs: Thursday 3pm-4pm and Friday 12pm-1pm in E51-242
- Bart: Thursday 1pm-2pm in E62-569

**Piazza**

This class uses Piazza when it comes to questions regarding problem sets, projects and class logistics.
- [Sign Up](https://piazza.com/mit/fall2023/15072)
- [Class Link](https://piazza.com/mit/fall2023/15072/home)

## Grading

1. (50%) Homework Assignments
2. (40%) Final Course Project
3. (10%) Class Participation

## Homework Assignments

| Number | Topic                                 | Post     | Due      |
|:-------|:--------------------------------------|:---------|:---------|
| 0      | Setting up R                          | Sept. 6  | Sept. 10 |
| 1      | Linear regression                     | Sept. 11 | Sept. 24 |
| 2      | Nonlinear Regression & Regularization | Sept. 25 | Oct. 8   |
| 3      | Logistic regression                   | Oct. 2   | Oct. 18  |
| 4      | CART & Random Forest                  | Oct. 18  | Oct. 31  |
| 5      | Clustering                            | Nov. 1   | Nov. 14  |
| 6      | Text Analytics & CF                   | Nov. 15  | Nov. 29  |


## Final Project

The students should organize themselves into groups of ideally four students and work on a final project. The following tentative timeline will be targetted.


1. **October 13**

By this date, each team needs to submit a one-page proposal that outlines a plan to apply analytical methods to a problem the team has identified using some of the concepts and tools discussed in the course. The proposal should include a description of: (1) the problem, (2) the data that you have or plan to collect to solve the problem, (3) which analytic techniques you plan to use, and (4) the impact or overall goal of the project (say, if you could build a perfect model, what would it be able to do?).

2. **October 20**

The teaching team will be available to answer questions over email and will provide all students with electronic feedback by this date.

3. **Week of November 6**

Each project team will set up a meeting with a member of the teaching team to show your progress in applying the analytical methods to your project topic. This meeting is intended to help you make progress on your project.

4. **December 3**

Each team will electronically submit a 1-page abstract summarizing their project (including the scope and idea of the project, what analytical methods/models were used, and what results were obtained).

5. **December 5**

The project abstracts will be uploaded to the class website and each student will vote on which projects they would like to see presented in class (since, unfortunately, due to time constraints, we will not be able to have all student teams present in class). The teaching team will vote as well.

6. **December 8**

Each team will electronically submit their final project report and 15 minutes presentation. This will consist of at most 4 pages of analysis and conclusions presented as a managerial memo but you can include appendices with supporting information.

7. **December 11&13**

ALL TEAMS should come to class prepared to give their 15-minute project presentation. The “winning” presenters will be notified in real-time during class.


## Tentative Lecture Outline

| Lecture | Date     | Topic                                                                                                         | Method                  |
|:--------|:---------|:--------------------------------------------------------------------------------------------------------------|-------------------------|
| 1       | Sept. 6  | [Introduction](https://vanparys-courses.gitlab.io/15.072-fs2023/L1-Introduction.pdf)                          | PageRank                |
| 2       | Sept. 11 | [Wine Quality](https://vanparys-courses.gitlab.io/15.072-fs2023/L2-Wine-Quality.pdf)                          | Linear regression       |
| 3       | Sept. 13 | [Smart Cities](https://vanparys-courses.gitlab.io/15.072-fs2023/L3-Mobility.pdf)                              | Nonlinear regression    |
|         | Sept. 18 | No Class                                                                                                      |                         |
| 4       | Sept. 20 | [Diabetes](https://vanparys-courses.gitlab.io/15.072-fs2023/L4-Diabetes.pdf)                                  | Regularization          |
| 5       | Sept. 25 | [Moneyball](https://vanparys-courses.gitlab.io/15.072-fs2023/L5-Moneyball.pdf)                                | Resampling              |
| 6       | Sept. 27 | [Churn Prediction](https://vanparys-courses.gitlab.io/15.072-fs2023/L6-Churn.pdf)                             | Logistic regression     |
| 7       | Oct. 2   | [Medical costs](https://vanparys-courses.gitlab.io/15.072-fs2023/L7-L8-CART.pdf)                              | CART                    |
| 8       | Oct. 4   | [Parole Decisions](https://vanparys-courses.gitlab.io/15.072-fs2023/L7-L8-CART.pdf)                           | CART                    |
|         | Oct. 9   | Indigenous Peoples' Day                                                                                       |                         |
| 9       | Oct. 11  | [Online Advertising](https://vanparys-courses.gitlab.io/15.072-fs2023/L9-CTR.pdf)                             | RF                      |
|         | Oct. 16  | No Class                                                                                                      |                         |
| 10      | Oct. 18  | [Boosting](https://vanparys-courses.gitlab.io/15.072-fs2023/L10-Boosting.pdf)                                 | Ensembles & Boosting    |
|         | Oct. 23  | LEAD week                                                                                                     |                         |
|         | Oct. 25  | LEAD week                                                                                                     |                         |
| 11      | Oct. 30  | [Costumer Segmentation](https://vanparys-courses.gitlab.io/15.072-fs2023/L11-Clustering.pdf)                  | Clustering              |
| 12      | Nov. 1   | [Twitter Analytics](https://vanparys-courses.gitlab.io/15.072-fs2023/L12-NLP.pdf)                             | Text Analytics          |
| 13      | Nov. 6   | [Netflix Prize](https://vanparys-courses.gitlab.io/15.072-fs2023/L13-Netflix.pdf		    )                 | Collaborative Filtering |
| 14      | Nov. 8   | [Social Network Analysis](https://vanparys-courses.gitlab.io/15.072-fs2023/L14-Networks.pdf)                  | Graphs                  |
| 15      | Nov. 13  | [Intro to NN](https://vanparys-courses.gitlab.io/15.072-fs2023/L15-NN.pdf)                                    | Neural Nets             |
| 16      | Nov. 15  | [Medical Diagnosis](https://vanparys-courses.gitlab.io/15.072-fs2023/L16-CNNs.pdf)                            | CNNs                    |
| 17      | Nov. 20  | [Experiments](https://vanparys-courses.gitlab.io/15.072-fs2023/L17-Experiments.pdf)                           | CRTs                    |
|         | Nov. 22  | Thanksgiving                                                                                                  |                         |
| 18      | Nov. 27  | [Parole decisions](https://vanparys-courses.gitlab.io/15.072-fs2023/L18-Fairness.pdf)                         | Fairness                |
| 19      | Nov. 29  | [Multimodality](https://vanparys-courses.gitlab.io/15.072-fs2023/L19-HAIM.pdf)                                | HAIM                    |
| 20      | Dec. 4   | [Mortality Prediction](https://vanparys-courses.gitlab.io/15.072-fs2023/L20-Potter.pdf)                       | Integer Optimization    |
| 21      | Dec. 6   | [Data Driven Optimization](https://vanparys-courses.gitlab.io/15.072-fs2023/L21-Data-Driven-Optimization.pdf) | Robust Optimization     |
|         | Dec. 11  | Project Presentations                                                                                         |                         |
|         | Dec. 13  | Project Presentations                                                                                         |                         |
