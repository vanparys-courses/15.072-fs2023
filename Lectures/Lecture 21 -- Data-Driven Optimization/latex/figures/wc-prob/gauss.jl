using LinearAlgebra
using JuMP
using Hypatia

using Polyhedra
using Plots

using DataFrames
using CSV

#=
FUNCTION chebyshev

INPUT :

mu, Sigma                    The mean and variance of the measures resp.
A, b                         Defines the polytop $C := \{x | Ax \leq b \}$
    
OUTPUT :

prob                         Probability of the complement of $C$
[px, X]                      Representation of worst-case measure

=#
function chebyshev(μ, S, A, b)

    K = size(A, 1)
    n = size(A, 2)

    model = Model(() -> Hypatia.Optimizer(verbose = false))

    λ = @variable(model, [1:K])
    z = [@variable(model, [1:n]) for _ in 1:K]
    Z = [@variable(model, [1:n, 1:n], PSD) for _ in 1:K]
    
    @objective(model, Max, sum(λ))

    for k in 1:K
        @constraint(model, dot(A[k, :], z[k]) >= b[k]*λ[k])
        @constraint(model, [Z[k] z[k]; z[k]' λ[k]] in PSDCone())
    end
    @constraint(model, sum([[Z[k] z[k]; z[k]' λ[k]] for k in 1:K]) <= [S μ; μ' 1], PSDCone())

    optimize!(model)
    
    λvalue = value.(λ)[findall(s->s>1e-4, value.(λ))]
    xvalue = hcat([value.(z[k])/value.(λ)[k] for k in findall(s->s>1e-4, value.(λ))]...)
    
    return objective_value(model), λvalue, xvalue
end

#=
FUNCTION gauss

INPUT :

mu, Sigma                    The mean and variance of the measures resp.
A, b                         Defines the polytop $C := \{x | Ax \leq b \}$
α                            Unimodality parameter
    
OUTPUT :

prob                         Probability of the complement of $C$
[px, X]                      Choquet representation of worst-case measure

=#
function gauss(μ, S, A, b, α)

    K = size(A, 1)
    n = size(A, 2)

    model = Model(() -> Hypatia.Optimizer(verbose = false))

    λ = @variable(model, [1:K])
    τ = @variable(model, [1:K])
    z = [@variable(model, [1:n]) for _ in 1:K]
    Z = [@variable(model, [1:n, 1:n], PSD) for _ in 1:K]
    
    @objective(model, Max, sum(λ)-sum(τ))

    for k in 1:K
        @constraint(model, [τ[k], dot(A[k, :], z[k]), λ[k]*b[k]^(α/(α+1))] in MOI.PowerCone(1/(α+1)))
        @constraint(model, [Z[k] z[k]; z[k]' λ[k]] in PSDCone())
    end
    @constraint(model, sum([[Z[k] z[k]; z[k]' λ[k]] for k in 1:K]) <= [(α+2)*S/α (α+1)*μ/α; (α+1)μ'/α 1], PSDCone())

    optimize!(model)
    
    λvalue = value.(λ)[findall(s->s>1e-4, value.(λ))]
    xvalue = hcat([value.(z[k])/value.(λ)[k] for k in findall(s->s>1e-4, value.(λ))]...)
    
    return objective_value(model), λvalue, xvalue
end


A = [1/sqrt(3) 1;
     -1/sqrt(6) 1;
     1 1;
     -1 0;
     1/sqrt(5) -1;
     -1/sqrt(3) -1]
b = [1, 1, 0.8, 0.8, 1, 1]

variance = DataFrame(x1=[0.05+0.3*cos(t) for t in 0:0.15:2*pi], x2=[-0.1+0.3*sin(t) for t in 0:0.15:2*pi], p=[0 for t in 0:0.15:2*pi])
CSV.write("variance.csv", variance)

# poly = polyhedron(hrep(A, b))
# vrep(poly)
# vertices = DataFrame(x1=poly.vrep.V[:, 1], x2=poly.vrep.V[:, 2])
# CSV.write("vertices.csv", vertices)

μ = [0.1, -0.2]/2
Σ = 0.2*I(2)
S = Σ+μ*μ'

# Chebyshev
p, λ, x = chebyshev(μ, S, A, b)

xcheb = DataFrame(x1=x[1, :], x2=x[2, :], p=λ/7)
CSV.write("xcheb.csv", xcheb)

# Gauss
p, λ, x = gauss(μ, S, A, b, 2)

xgauss = DataFrame(x1=x[1, :], x2=x[2, :], p=λ/7)
CSV.write("xgauss.csv", xgauss)
