using Convex
using Mosek
using MosekTools
using Plots
using Random
using SpecialFunctions
Random.seed!(277)
using DataFrames
using CSV

# CDF of normal distribution
#
# x : position
# θ : location parameter
# σ : standard deviation
function cdf(x, θ, σ)
    return 1/2*(1+erf((x-θ)/(σ*sqrt(2))))
end

# Probability of intervals defined by xs
#
#
# θ : location parameter
# σ : standard deviation
# xs: vector of locations
function GN(θ, σ, xs)
    PR = zeros(length(xs))
    PR[1] = cdf(xs[1], θ, σ)
    for i in 2:length(xs)-1
        PR[i] = cdf(xs[i], θ, σ) - cdf(xs[i-1], θ, σ)
    end
    PR[end] = 1 - cdf(xs[end-1], θ, σ)
    return PR
end

# Define reference distributions
xs = -10:0.25:10
μ = 0.6*GN(-4, 1, xs)+0.4*GN(0.5, 2.5, xs)
ν = 0.45*GN(-3, 2, xs)+0.55*GN(3, 1.65, xs)
ϵ=4

CSV.write("mu.csv", DataFrame(x=xs, y=μ))
CSV.write("nu.csv", DataFrame(x=xs, y=ν))

T = Convex.Variable(length(xs), length(xs))
constrs = [T >= 0, sum(T)==1]
constrs = constrs + [sum(T, dims=1)==μ', sum(T, dims=2)==ν]
obj = sum([(norm(xs[i]-xs[j])>=ϵ)*T[i, j] + 2*relative_entropy(T[i, j], Constant(μ[i]*ν[j]))
           for i in 1:length(xs), j in 1:length(xs)])

problem = minimize(obj, constrs)
solve!(problem, Mosek.Optimizer)

df_T = DataFrame(x=repeat(xs, inner=length(xs)), y=repeat(xs, outer=length(xs)), T=collect(T.value[:]), C = collect(T.value[:])/maximum(collect(T.value[:])))
CSV.write("T.csv", df_T)

df_D = DataFrame(x=repeat(xs, inner=length(xs)), y=repeat(xs, outer=length(xs)), D=abs.(repeat(xs, inner=length(xs)).-repeat(xs, outer=length(xs))))
CSV.write("D.csv", df_D)

