(TeX-add-style-hook
 "nonsmooth-dro"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "import"
    "pgfplots"
    "anyfontsize"))
 :latex)

