(TeX-add-style-hook
 "statistical-error"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "import"
    "pgfplots"))
 :latex)

