(TeX-add-style-hook
 "preamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("tcolorbox" "most") ("circuitikz" "siunitx")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "framed"
    "amsmath"
    "amsthm"
    "amssymb"
    "bbold"
    "mathtools"
    "xcolor"
    "capt-of"
    "hyperref"
    "tcolorbox"
    "fancybox"
    "acronym"
    "tikz"
    "pgfplots"
    "circuitikz")
   (TeX-add-symbols
    '("e" 1)
    '("Prob" 0)
    '("kd" 0)
    '("rf" 0)
    '("as" 0)
    '("ml" 0)
    '("LP" 2)
    '("KL" 0)
    '("defn" 0)
    '("tset" 2)
    '("set" 2)
    '("tfrac" 2)
    '("one" 1)
    '("E" 2)
    '("abs" 1)
    '("iprod" 2)
    '("ceil" 1)
    '("floor" 1)
    '("norm" 1)
    '("green" 1)
    '("blue" 1)
    '("gray" 1)
    '("red" 1)
    "tpose"
    "mc"
    "mb"
    "mf"
    "ms"
    "mr"
    "N"
    "Cx"
    "nRe"
    "st"
    "Q"
    "cl"
    "interior"
    "relint"
    "bd"
    "ex"
    "rank"
    "epi"
    "dom"
    "tr"
    "diag"
    "prox"
    "conv"
    "cone"
    "aff"
    "minimize"
    "maximize"
    "vec")
   (LaTeX-add-xcolor-definecolors
    "red"
    "gray"
    "blue"
    "green")
   (LaTeX-add-tcolorbox-newtcboxes
    '("tshadowbox" "1" "[" ""))
   (LaTeX-add-acronyms
    "wlog"
    "lsc"))
 :latex)

