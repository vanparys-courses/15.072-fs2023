using DataFrames
using CSV

X = [range(0,1/4,length=100),
     range(1/4,2/4,length=100),
     range(2/4,3/4,length=100),
     range(3/4,4/4,length=100)]

#####################
## Discontinuous f ##
#####################

f01 = DataFrame(x=X[1], y=sin.(X[1]))
f02 = DataFrame(x=X[2], y=sin.((X[2].-1).^2))
f03 = DataFrame(x=X[3], y=0 .+ X[3])
f04 = DataFrame(x=X[4], y=sqrt.(X[4]))

CSV.write("f01.csv", f01)
CSV.write("f02.csv", f02)
CSV.write("f03.csv", f03)
CSV.write("f04.csv", f04)

######################
## Discontinuous f' ##
######################

f11 = DataFrame(x=X[1], y=2*X[1])
f12 = DataFrame(x=X[2], y=1 .- 2*X[2])
f13 = DataFrame(x=X[3], y=8*(X[3].-0.5).^2)
f14 = DataFrame(x=X[4], y=1/2)

CSV.write("f11.csv", f11)
CSV.write("f12.csv", f12)
CSV.write("f13.csv", f13)
CSV.write("f14.csv", f14)

#######################
## Discontinuous f'' ##
#######################

f21 = DataFrame(x=X[1], y=1/2)
f22 = DataFrame(x=X[2], y=1/2 .+ 2(X[2].-1/4).^2)
f23 = DataFrame(x=X[3], y=2*0.3125 .+ (X[3].-0.5))
f24 = DataFrame(x=X[4], y=2*0.4375 .- 6*(X[4].-0.75).^2 .+ (X[4].-0.75) )

CSV.write("f21.csv", f21)
CSV.write("f22.csv", f22)
CSV.write("f23.csv", f23)
CSV.write("f24.csv", f24)
