library(mlr)
library(OOBCurve)
library(tidyverse)
library(lubridate)
library(dplyr)
library(purrr)
library(ggplot2)
library(scales)
library(stringr)
library(tm)
library(caret)
library(rpart)
library(rpart.plot)
library(randomForest)
library(data.table)
library(ROCR)
library(leaps)
library(xgboost)
library(tidytext)

#read in data
tweets.trump <-read.csv("trump_tweets.csv", stringsAsFactors = FALSE)
### Definition and restriction of corpus of words

tweets.trump <- tweets.trump %>% select(-X)

# Definition of corpus of words
corpus = Corpus(VectorSource(tweets.trump$text))

# corpus is a list with an entry for each tweet
# Each entry, in turn, is made up of two components
# the 'content' component shows the actual text of the tweet
corpus[[1]]$content

# the 'meta' component shows whatever metadata R has been able
# to figure out
# in this case, it was able to detect a date/timestamp and the language
# but nothing else
corpus[[1]]$meta


# We will now use the tm_map function to transform the tweets,
# step by step

#1. Make everything lower case
# we use the built-in 'tolower' function to do this

corpus = tm_map(corpus, tolower)
# You can see how the first tweet is all lower-case now
corpus[[1]]$content

#2. Replace "https://link" with "https link" to make sure 'https' is picked up as a single word
f <- content_transformer(function(x, oldtext,newtext) gsub(oldtext, newtext, x))
corpus <- tm_map(corpus, f, "https://", "http ")

# 3. Remove punctuation
corpus <- tm_map(corpus, removePunctuation)

# 4. Remove stop words
corpus = tm_map(corpus, removeWords, stopwords("english"))

# 5. Remove other words manually
corpus = tm_map(corpus, removeWords, c("realdonaldtrump", "donaldtrump"))

# Stemming
corpus = tm_map(corpus, stemDocument)

# Find high-frequency words and remove uncommon words
frequencies = DocumentTermMatrix(corpus)

# Just for fun, let's list the words that occur at least 200 times
findFreqTerms(frequencies, lowfreq=200)

# Let's remove words that occur less than 1% of the time
sparse = removeSparseTerms(frequencies, 0.99)

# Fraction of dataset used in the analysis
(pct.text <- sum(as.matrix(sparse)) / sum(as.matrix(frequencies)))

### Add dependent variable back into dataset

# Creation dataset and importation of dependent variable
documentterms = as.data.frame(as.matrix(sparse))
documentterms$TrumpWrote = tweets.trump$TrumpWrote

# Let's move the dependent variable to the first column
# Note the use of the handy 'everything()' function to refer to 
# all the other variables

documentterms <- documentterms %>% select(TrumpWrote, everything())

# Changing two columns names to avoid interference with built-in functions
# since it is problematic to have numbers as column names (2016)
# and function names as column names (next)

documentterms %>% rename(y2016 = '2016', NEXT = 'next') -> documentterms

### Splitting into training and test set
# the 'ymd_hms' function tranforms the created_at column into actual dates so they can
# be compared correctly with June 1, 2016

split1 = (tweets.trump$created_at < "2016-06-01")
split2 = (tweets.trump$created_at >= "2016-06-01")

train = documentterms[split1,]
test = documentterms[split2,]

##### Baseline: Predicting all tweets coming from Trump himself

perf.summary <- data.frame(Model = "Baseline",
                           Accuracy = mean(test$TrumpWrote) %>% round(2),
                           TPR = 1,
                           FPR = 1,
                           AUC = 0.5)

##### BAG OF WORDS

# First, let's write a couple of helper functions
# to avoid writing the same code again and again

# Calc accuracy, TPR and FPR
calc_key_metrics <- function(preds, actuals) {
  if (is.factor(preds)) matrix = table(actuals, preds)
  else matrix = table(actuals, preds > 0.5)
  accuracy = sum(diag(matrix))/sum(matrix) 
  TPR = (matrix[2,2])/sum(matrix[2,]) 
  FPR = (matrix[1,2])/sum(matrix[1,])
  return(list(Accuracy = accuracy,
              TPR = TPR,
              FPR = FPR ))
}

# Generate points for the ROC Curve
make_ROC_df <- function(ROC) {
  data.frame(fpr=slot(ROCR::performance(ROC, "tpr", "fpr"),"x.values")[[1]],
             tpr=slot(ROCR::performance(ROC, "tpr", "fpr"),"y.values")[[1]])
}

### Logistic regression

logreg = glm(TrumpWrote ~., data=train, family="binomial")
summary(logreg)

preds <- predict(logreg, newdata=test, type="response")
ROC <- prediction(preds,test$TrumpWrote) 
AUC <- ROCR::performance(ROC, "auc")@y.values[[1]] 

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "Logistic",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.logreg.df <- make_ROC_df(ROC)

### CART

cart = rpart(TrumpWrote ~ ., data=train, method="class", cp = .003)

prp(cart, digits=3, split.font=1, varlen = 0, faclen = 0)

preds <- predict(cart, newdata=test, type="class")
ROC <- prediction(predict(cart, newdata=test)[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CART",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.cart.df <- make_ROC_df(ROC)

### Random Forest
train$TrumpWrote = as.factor(train$TrumpWrote)
test$TrumpWrote = as.factor(test$TrumpWrote)

set.seed(123)
rfmodel = randomForest(TrumpWrote ~., data=train)

importance.rf <- data.frame(imp=round(importance(rfmodel)[order(-importance(rfmodel)),],2))

preds = predict(rfmodel, newdata=test)
ROC <- prediction(predict(rfmodel,newdata=test,type="prob")[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "RF",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.RF.df <- make_ROC_df(ROC)

### Summary so far

perf.summary

print(ggplot() +
        geom_line(data=ROC.logreg.df,aes(x=fpr,y=tpr,colour="a"),lwd=1,lty=1) +
        geom_line(data=ROC.cart.df,aes(x=fpr,y=tpr,colour="b"),lwd=1,lty=2) +
        geom_line(data=ROC.RF.df,aes(x=fpr,y=tpr,colour="c"),lwd=1,lty=3) +
        xlab("False Positive Rate") +
        ylab("True Positive Rate") +
        theme_bw() +
        xlim(0, 1) +
        ylim(0, 1) +
        scale_color_manual(name="Method", labels=c("a"="Logistic regression", "b"="CART", "c"="Random forest"), values=c("a"="gray", "b"="blue", "c"="red")) +
        theme(axis.title=element_text(size=18), axis.text=element_text(size=18), legend.text=element_text(size=18), legend.title=element_text(size=18)))

##### ADD METADATA

### Update to dataset

# Add for presence of picture  
tweets.trump = tweets.trump %>%
  mutate(pic_link = ifelse(str_detect(text, "t.co"),1,0))
# Add tweet length
tweets.trump = tweets.trump %>%
  mutate(length = nchar(text))
# Add hour of day
tweets.trump = tweets.trump %>%
  mutate(hour = hour(ymd_hms(created_at)))
  # mutate(hour = hour(with_tz(created_at, "EST")))
# Add number of hashtags
tweets.trump = tweets.trump %>%
  mutate(hashtag = str_count(text, "#"))
# Add number of @mentions
tweets.trump = tweets.trump %>%
  mutate(mentions = str_count(text, "@"))

# Complete dataset
documentterms$pic_link = tweets.trump$pic_link
documentterms$length = tweets.trump$length
documentterms$hour = as.factor(tweets.trump$hour)
documentterms$hashtag = tweets.trump$hashtag
documentterms$mentions = tweets.trump$mentions


train = documentterms[split1,]
test = documentterms[split2,]

### Rinse and repeat

### Logistic regression

logreg = glm(TrumpWrote ~., data=train, family="binomial")
summary(logreg)

preds <- predict(logreg, newdata=test, type="response")
ROC <- prediction(preds,test$TrumpWrote) 
AUC <- ROCR::performance(ROC, "auc")@y.values[[1]] 

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "Logistic w meta",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.logreg.df <- make_ROC_df(ROC)

### CART

cart = rpart(TrumpWrote ~ ., data=train, method="class", cp = .003)

prp(cart, digits=3, split.font=1, varlen = 0, faclen = 0)

preds <- predict(cart, newdata=test, type="class")
ROC <- prediction(predict(cart, newdata=test)[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CART w meta",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.cart.df <- make_ROC_df(ROC)

### Random Forest
train$TrumpWrote = as.factor(train$TrumpWrote)
test$TrumpWrote = as.factor(test$TrumpWrote)

set.seed(123)
rfmodel = randomForest(TrumpWrote ~., data=train)

importance.rf <- data.frame(imp=round(importance(rfmodel)[order(-importance(rfmodel)),],2))

preds = predict(rfmodel, newdata=test)
ROC <- prediction(predict(rfmodel,newdata=test,type="prob")[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "RF w meta",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.RF.df <- make_ROC_df(ROC)

### Summary after adding metadata

perf.summary


print(ggplot() +
        geom_line(data=ROC.logreg.df,aes(x=fpr,y=tpr,colour="a"),lwd=1,lty=1) +
        geom_line(data=ROC.cart.df,aes(x=fpr,y=tpr,colour="b"),lwd=1,lty=2) +
        geom_line(data=ROC.RF.df,aes(x=fpr,y=tpr,colour="c"),lwd=1,lty=3) +
        xlab("False Positive Rate") +
        ylab("True Positive Rate") +
        theme_bw() +
        xlim(0, 1) +
        ylim(0, 1) +
        scale_color_manual(name="Method", labels=c("a"="Logistic regression", "b"="CART", "c"="Random forest"), values=c("a"="gray", "b"="blue", "c"="red")) +
        theme(axis.title=element_text(size=18), axis.text=element_text(size=18), legend.text=element_text(size=18), legend.title=element_text(size=18)))


##### Sentiment analysis

### Update of dataset

# In the newest version of tidytext, the NRC lexicon was removed due to licensing issues.
# Until these issues get resolved, this workaround downloads the NRC dataset directly.
# https://saifmohammad.com/WebPages/lexicons.html
# Use of "Non-commercial Research Use" option

nrc <- read.csv('NRC-Emotion-Lexicon-Wordlevel-v0.92.csv', header=FALSE)
colnames(nrc) <- c('word', 'sentiment', 'indicator')
nrc <- nrc %>%
  filter(indicator==1) %>%
  dplyr::select(word, sentiment)

# convert from 'long' format to 'wide' format 
# using the 'dcast' function dfrom the data.table package
# compare 'nrc' and 'nrc_wide' to see what this does
nrc_wide <- dcast(nrc, word ~ sentiment)

# convert all columns except 'word'in nrc_wide to be 0 and 1 values
nrc_wide %>% mutate_at(vars(-word),
                       function(x) ifelse(is.na(x),0,1)) -> nrc_wide

# check an example to see if we did this right
nrc %>% filter(word=="lawful")
nrc_wide %>% filter(word=="lawful")


# take tweets, break into one word per line, so tweetid - word
reg <- "([^A-Za-z\\d#@']|'(?![A-Za-z\\d#@]))"
tweet_words <- tweets.trump %>%
  filter(!str_detect(text, '^"')) %>%
  mutate(text = str_replace_all(text, "https://t.co/[A-Za-z\\d]+|&amp;", "")) %>%
  mutate(text = str_replace_all(text, "'", "")) %>%
  unnest_tokens(word, text, token = "regex", pattern = reg) %>%
  filter(!word %in% stop_words$word,
         str_detect(word, "[a-z]")) %>%
  count(word, TrumpWrote, created_at)

# inner join
# sum over rows so (max) one row per tweetid with totals of sentiments
joined = inner_join(tweet_words, nrc_wide) %>%
  group_by(created_at) %>%
  summarise(anger = sum(anger), anticipation = sum(anticipation), disgust = sum(disgust), fear = sum(fear), joy = sum(joy),
            negative = sum(negative), positive = sum(positive), sadness = sum(sadness), surprise = sum(surprise), trust = sum(trust)) 

joined.full = full_join(tweets.trump,joined, by="created_at")

# In all columns except 1:8, replace NAs with 0s
joined.full %>% mutate_at(vars(-(1:8)),
                       function(x) ifelse(is.na(x),0,x)) -> joined.full


# Let's add sentiment predictors to the dataset
documentterms <- cbind(documentterms, joined.full %>% select(-(1:8)))

### Split into training and test set
train = documentterms[split1,]
test = documentterms[split2,]

### Rinse and repeat

### Logistic regression

logreg = glm(TrumpWrote ~., data=train, family="binomial")
summary(logreg)

preds <- predict(logreg, newdata=test, type="response")
ROC <- prediction(preds,test$TrumpWrote) 
AUC <- ROCR::performance(ROC, "auc")@y.values[[1]] 

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "Logistic w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.logreg.df <- make_ROC_df(ROC)

### CART

cart = rpart(TrumpWrote ~ ., data=train, method="class", cp = .003)

prp(cart, digits=3, split.font=1, varlen = 0, faclen = 0)

preds <- predict(cart, newdata=test, type="class")
ROC <- prediction(predict(cart, newdata=test)[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CART w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.cart.df <- make_ROC_df(ROC)

### Random Forest
train$TrumpWrote = as.factor(train$TrumpWrote)
test$TrumpWrote = as.factor(test$TrumpWrote)

set.seed(123)
rfmodel = randomForest(TrumpWrote ~., data=train)

importance.rf <- data.frame(imp=round(importance(rfmodel)[order(-importance(rfmodel)),],2))

preds = predict(rfmodel, newdata=test)
ROC <- prediction(predict(rfmodel,newdata=test,type="prob")[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "RF w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.RF.df <- make_ROC_df(ROC)

### Summary after adding metadata and sentiment predictors

perf.summary

print(ggplot() +
        geom_line(data=ROC.logreg.df,aes(x=fpr,y=tpr,colour="a"),lwd=1,lty=1) +
        geom_line(data=ROC.cart.df,aes(x=fpr,y=tpr,colour="b"),lwd=1,lty=2) +
        geom_line(data=ROC.RF.df,aes(x=fpr,y=tpr,colour="c"),lwd=1,lty=3) +
        xlab("False Positive Rate") +
        ylab("True Positive Rate") +
        theme_bw() +
        xlim(0, 1) +
        ylim(0, 1) +
        scale_color_manual(name="Method", labels=c("a"="Logistic regression", "b"="CART", "c"="Random forest"), values=c("a"="gray", "b"="blue", "c"="red")) +
        theme(axis.title=element_text(size=18), axis.text=element_text(size=18), legend.text=element_text(size=18), legend.title=element_text(size=18)))


#
#
#
# Let's now use cross-validation to increase AUC as much as we can
#
#
#

#### Improve predictors with cross-validation/oob
train$TrumpWrote = as.factor(make.names(as.factor(train$TrumpWrote)))
test$TrumpWrote = as.factor(make.names(as.factor(test$TrumpWrote)))

#CART - cross validate CP

set.seed(100)

trControl <- trainControl(method="cv",
                          number=10,  # use 10 folds
                          summaryFunction=twoClassSummary, classProbs = T) 

cpCV = caret::train(TrumpWrote~., 
             data = train,
             trControl = trControl, 
             metric = "ROC", 
             maximize = T,
             method = "rpart",
             tuneGrid = data.frame(cp = seq(.0001, .005, by = .0001)))

cpbest = cpCV$bestTune
#0.0013
cart <- rpart(factor(TrumpWrote) ~ ., 
                                data=train, 
                                method="class", 
                                cp = .0013)

prp(cart, digits=3, split.font=1, varlen = 0, faclen = 0)

preds <- predict(cart, newdata=test, type="class")
ROC <- prediction(predict(cart, newdata=test)[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CV CART w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.cart.df <- make_ROC_df(ROC)


#
# Let's cross-validate RFs
#

grid =data.frame(mtry = seq(8,  60, 2))

#With out of bag training train() can't accept custom summary measures, 
#so can't train using auc as the metric. Need to do it ourselves

set.seed(100)
auc.mtry =rep(0, nrow(grid))
for(i in 1:nrow(grid))
{  
  mod = randomForest(TrumpWrote~., 
                     data = train, 
                     mtry = grid$mtry[i], 
                     keep.inbag = T, 
                     ntree = 500)
  auc.mtry[i] = OOBCurve(mod, 
                         data = train, 
                         task = makeClassifTask(data=train, target = "TrumpWrote"))[500,1]
  print(i)
}

cvmtry = grid$mtry[which.max(auc.mtry)]

#34
set.seed(123)
rfmodel <- randomForest(TrumpWrote ~., data=train, mtry = 34)

importance.rf <- data.frame(imp=round(importance(rfmodel)[order(-importance(rfmodel)),],2))

preds = predict(rfmodel, newdata=test)
ROC <- prediction(predict(rfmodel,newdata=test,type="prob")[,2], test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CV RF w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.RF.df <- make_ROC_df(ROC)

#
# Finally, let's try XGBoost!
#


ytrain = ifelse(train$TrumpWrote=="X1", 1, 0)
xtrain = data.matrix(train%>%select(-TrumpWrote))
xtest = data.matrix(test%>%select(-TrumpWrote))
ytest = ifelse(test$TrumpWrote=="X1", 1, 0)



tg = expand.grid(max_depth = 5:12,
                 eta = .02, 
                 subsample=c(.5, .8), 
                 min_child_weight=1, 
                 gamma=c(0, .001),  
                 colsample_bytree=1, 
                 alpha=c(0,.5,1), 
                 lambda=c(0,.5,1))       


round.best = rep(0, nrow(tg))
b =    Sys.time() 
auc.cv = rep(0, nrow(tg))
set.seed(250)

for(i in 1:nrow(tg))
{
  params.new = split(t(tg[i,]), colnames(tg))
  eval = xgb.cv(data = as.matrix(xtrain), label = ytrain, params = params.new, nrounds = 1000, nfold = 5, metrics = c("auc"), verbose = F, early_stopping_rounds = 10)$evaluation_log$test_auc_mean
  round.best[i] = which.max(eval)
  auc.cv[i] = eval[round.best[i]]
  
  print(i)
}
Sys.time()
auc.cv
winner = which.max(auc.cv)
tg[winner,]
round.best[winner]
#     max_depth  eta subsample min_child_weight gamma colsample_bytree alpha lambda
#266         6 0.02       0.8                1     0                1     1      1
#> round.best[winner]
#[1] 232
#xgboost
set.seed(2000)
xgb_model <- xgboost(data = xtrain, 
                     label = ytrain, 
                     nrounds = 232, 
                     params = list(objective= "reg:logistic", 
                                   eta = .02, 
                                   max_depth = 6,
                                   subsample = .8, 
                                   gamma = 0, 
                                   lambda = 1, 
                                   alpha = 1))

xgb.plot.importance(xgb.importance(model = xgb_model))

preds <- predict(xgb_model, newdata = xtest)
ROC <- prediction(preds, test$TrumpWrote)
AUC <- as.numeric(ROCR::performance(ROC, "auc")@y.values)

metrics <- calc_key_metrics(preds, test$TrumpWrote)

perf.summary %>% add_row(Model = "CV XGB w meta+sentiment",
                         Accuracy = metrics$Accuracy,
                         TPR = metrics$TPR,
                         FPR = metrics$FPR,
                         AUC = AUC) -> perf.summary

ROC.XGB.df <- make_ROC_df(ROC)


#
#
#

# Final summary of everything we have tried

perf.summary

# The ROC curves for the CV models

print(ggplot() +
        geom_line(data=ROC.logreg.df,aes(x=fpr,y=tpr,colour="a"),lwd=1,lty=1) +
        geom_line(data=ROC.cart.df,aes(x=fpr,y=tpr,colour="b"),lwd=1,lty=2) +
        geom_line(data=ROC.RF.df,aes(x=fpr,y=tpr,colour="c"),lwd=1,lty=3) +
        geom_line(data=ROC.XGB.df,aes(x=fpr,y=tpr,colour="d"),lwd=1,lty=4) +
        xlab("False Positive Rate") +
        ylab("True Positive Rate") +
        theme_bw() +
        xlim(0, 1) +
        ylim(0, 1) +
        scale_color_manual(name="Method", labels=c("a"="Logistic regression", "b"="CART", "c"="Random forest", "d"= "XGBoost"), values=c("a"="gray", "b"="blue", "c"="red", "d" = "darkgreen")) +
        theme(axis.title=element_text(size=18), axis.text=element_text(size=18), legend.text=element_text(size=18), legend.title=element_text(size=18)))
